## Handover Bot
This 🤖 creates and assigns the triage issue to corresponding DRIs and pings the next week's DRIs as well, 
according to the Rotation Schedule. The issue is 
[scheduled](https://gitlab.com/gitlab-org/quality/pipeline-triage/-/pipeline_schedules) 
to open on Saturdays prior upcoming workweek.

It is also carrying over the issues from last week to contextualise the current DRI of past failures. 
The carryovers are issues that remained opened since reported on the previous week. 
These are automatically generated and included in the Pipeline Triage Issue.

## DRI Gem
Looking for some help triaging failures this week? Take a look at the 
[dri gem](https://gitlab.com/gitlab-org/quality/dri) to help reporting to this project your triaging progress.

## DRI weekly [rotation] schedule

|**Start Date**|**APAC [DRI]**|**EMEA [DRI]**|**AMER [DRI]**|
|:-|:-|:-|:-|
|2023-10-02|Sanad Liaquat|Nick Westbury|Tiffany Rea|
|2023-10-09|Vishal Patel|Sofia Vistas|Andy Hohenner|
|2023-10-16|Nivetha Prabakaran|Grant Young|Désirée Chevalier|
|2023-10-23|Mark Lapierre|Andrejs Cunskis|Dan Davison|
|:point_right: **2023-10-30**|**Jay McCure**|**Will Meek**|**Richard Chong**|
|2023-11-06|Harsha Muralidhar|Nailia Iskhakova|Valerie Burton|
|2023-11-13|Sanad Liaquat|John McDonnell|Zeff Morgan|
|2023-11-20|Jay McCure|Nick Westbury|Brittany Wilkerson|
|2023-11-27|Nivetha Prabakaran|Sofia Vistas|Tiffany Rea|
|2023-12-04|Mark Lapierre|Grant Young|Andy Hohenner|
|2023-12-11|Jay McCure|Andrejs Cunskis|Désirée Chevalier|
|2023-12-18|Harsha Muralidhar|Will Meek|Dan Davison|
|2023-12-25|Sanad Liaquat|John McDonnell|Richard Chong|
|2024-01-01|Vishal Patel|Nailia Iskhakova|Valerie Burton|
|2024-01-08|Nivetha Prabakaran|Nick Westbury|Zeff Morgan|
|2024-01-15|Mark Lapierre|Sofia Vistas|Brittany Wilkerson|
|2024-01-22|Vishal Patel|Grant Young|Tiffany Rea|
|2024-01-29|Harsha Muralidhar|Andrejs Cunskis|Andy Hohenner|

[DRI]: https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/
[rotation]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/oncall-rotation/

_Any changes made to this `README.md` will get overwritten weekly by DRI highlighter task._
