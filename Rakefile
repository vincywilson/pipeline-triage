# frozen_string_literal: true

require_relative 'scheduling'
require_relative 'dri_highlighter'

desc "Create and submit new pipeline triage schedule between today and no_of_days"
task :create_and_submit_schedule, [:no_of_days] do |t, args|
  args.with_defaults(:no_of_days => 90)
  schedule = Scheduling.new(args[:no_of_days])
  schedule.populate_schedule
  schedule.submit_schedule
end

desc "Create and print out to console new pipeline triage schedule between today and no_of_days"
task :create_schedule_only, [:no_of_days] do |t, args|
  args.with_defaults(:no_of_days => 90)
  Scheduling.new(args[:no_of_days]).populate_schedule
end

desc "Locate/highlight current week DRI in schedule then update README"
task :dri_highlighter do |t|
  DriHighlighter.new.run
end
