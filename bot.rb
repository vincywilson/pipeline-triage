#!/usr/bin/env ruby

require './handover'

handover = Handover.new

current_dris = handover.current_dris
next_dris = handover.next_dris
assignees = handover.assignee_ids
timezone_dris = handover.timezone_dris_table
carryovers = handover.carryovers

puts "🤖: This week's expected DRIs are: #{current_dris.map { |k, v| v[:handle] }}"
puts "🤖: Next week's expected DRIs are: #{next_dris.map { |k, v| v[:handle] }}"
puts "🤖: There are no carryovers from last week..." if carryovers.empty?
puts "🤖: There are #{carryovers.size} carryovers from last week"

template = <<-EOF
# DRI

Please review the [responsiblities](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/oncall-rotation/#responsibility) and [guidelines](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures/) if you have not done so recently.

Above all else, please remember that the aim of pipeline triage is to identify problems and try to get them **resolved** (ideally) _before_ they impact users.

## Resources

- [Debugging Failing Tests and Test Pipelines](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/debugging-qa-test-failures)
- The [dri gem](https://gitlab.com/gitlab-org/quality/dri) (to help report and triage issues)
- [Quality runbooks](https://gitlab.com/gitlab-org/quality/runbooks) (feel free to add more)
- [Test execution dashboards](https://dashboards.quality.gitlab.net/)
- [Recently updated failure issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_desc&state=opened&or%5Blabel_name%5D%5B%5D=failure%3A%3Abroken-test&or%5Blabel_name%5D%5B%5D=failure%3A%3Abug&or%5Blabel_name%5D%5B%5D=failure%3A%3Anew&or%5Blabel_name%5D%5B%5D=failure%3A%3Ainvestigating&or%5Blabel_name%5D%5B%5D=failure%3A%3Atest-environment&or%5Blabel_name%5D%5B%5D=failure%3A%3Aflaky-test&or%5Blabel_name%5D%5B%5D=failure%3A%3Astale-test&first_page_size=100)
- [Non-quarantined test results in the testcases project](https://gitlab.com/gitlab-org/quality/testcases/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=status%3A%3Aautomated&not%5Blabel_name%5D%5B%5D=quarantine&first_page_size=100)

#{timezone_dris}

# Carryovers

#{carryovers.join("\n")}

# [Current known issues for CustomersDot project](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Quality%3Ae2e-test&label_name[]=staging%3A%3Afailed)

/label ~Quality

/assign
EOF

handover.new_triage_report(template, assignees)
puts "🤖: Hooray, new triage report open at https://gitlab.com/gitlab-org/quality/pipeline-triage/-/issues 🎉"

handover.close_past_triage_report
puts "🤖: Closed last week triage issue. Bye 👋"
