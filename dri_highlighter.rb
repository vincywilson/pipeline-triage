# frozen_string_literal: true

require './client'
require 'markdown-tables'
require 'yaml'

# This script is to read the DRI schedule in README.md
# Find the current week row in the table
# Highly it and update README.md with the changes
#
# rake dri_highlighter

class DriHighlighter
  def initialize
    @schedule = YAML.load_file('new_dri_schedule.yml', permitted_classes: [Date])
  end

  def run
    locate_and_highlight
    commit_and_merge_changes
  end

  private

  def locate_and_highlight
    populate_schedule_with_new_highlight
    generate_new_table
  end

  def current_week
    # Scheduled pipeline is run on Saturday so
    # we are looking for the first row with
    # date after the date of pipeline execution
    #
    @schedule.find do |row|
      Date.parse(row[0].to_s) > Date.today
    end
  end

  def populate_schedule_with_new_highlight
    @schedule.each_with_index do |row, index|
      if row == current_week
        @schedule[index] = [
          ":point_right: **#{current_week[0]}**",
          "**#{current_week[1]}**",
          "**#{current_week[2]}**",
          "**#{current_week[3]}**"
        ]
      else
        @schedule[index] = [row[0].to_s, row[1], row[2], row[3]]
      end
    end
  end

  def generate_new_table
    headers = ['**Start Date**', '**APAC [DRI]**', '**EMEA [DRI]**', '**AMER [DRI]**']

    # Align left
    @table = MarkdownTables.make_table(headers, @schedule, is_rows: true, align: %w[l l l l])
    puts "\nNew schedule from #{@schedule.first[0]} to #{@schedule.last[0]}\n\n#{@table}\n\n"
  end

  def new_readme_content
    <<~EOF
      ## Handover Bot
      This 🤖 creates and assigns the triage issue to corresponding DRIs and pings the next week's DRIs as well, 
      according to the Rotation Schedule. The issue is 
      [scheduled](https://gitlab.com/gitlab-org/quality/pipeline-triage/-/pipeline_schedules) 
      to open on Saturdays prior upcoming workweek.
      
      It is also carrying over the issues from last week to contextualise the current DRI of past failures. 
      The carryovers are issues that remained opened since reported on the previous week. 
      These are automatically generated and included in the Pipeline Triage Issue.
      
      ## DRI Gem
      Looking for some help triaging failures this week? Take a look at the 
      [dri gem](https://gitlab.com/gitlab-org/quality/dri) to help reporting to this project your triaging progress.
      
      ## DRI weekly [rotation] schedule

      #{@table}
      
      [DRI]: https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/
      [rotation]: https://about.gitlab.com/handbook/engineering/quality/quality-engineering/oncall-rotation/

      _Any changes made to this `README.md` will get overwritten weekly by DRI highlighter task._
    EOF
  end

  # Required: Environment variable ACCESS_TOKEN=<your gitlab personal access token>
  #
  def commit_and_merge_changes
    @client = Client.new

    @client.create_branch(new_branch_name, 'master')
    @client.create_commit(new_branch_name, 'Highlight schedule with current DRI', commit_actions)
    mr = @client.create_merge_request('', mr_title, new_branch_name)

    sleep 10 # Give the MR some time to be ready
    @client.accept_merge_request(mr["iid"].to_s)
  end

  def commit_actions
    [
      {
        action: 'update',
        file_path: 'README.md',
        content: new_readme_content
      }
    ]
  end

  def new_branch_name
    "#{Date.today}-locate-current-dri"
  end

  def mr_title
    "[#{Date.today}] Highlight schedule with current DRI"
  end
end
