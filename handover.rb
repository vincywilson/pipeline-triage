# frozen_string_literal: true

require './client'
require 'json'
require 'uri'
require 'markdown-tables'
require 'yaml'

class Handover
  def initialize
    @future_schedule = future_schedule
    @name_list = YAML.load_file('SETs_name_list.yml')
    @client = Client.new
  end

  def future_schedule
    future_schedule = YAML.load_file('new_dri_schedule.yml', permitted_classes: [Date])

    future_schedule.reject! do |row|
      Date.parse(row[0].to_s) < Date.today
    end

    future_schedule
  end

  def start_date
    @future_schedule.first[0]
  end

  def end_date
    converted_start_date = Date::strptime(start_date.to_s, "%Y-%m-%d")
    (converted_start_date + 5).to_s
  end

  def next_start_date
    @future_schedule[1][0]
  end

  def apac_dri
    @future_schedule.first[1]
  end

  def emea_dri
    @future_schedule.first[2]
  end

  def amer_dri
    @future_schedule.first[3]
  end

  def next_apac_dri
    @future_schedule[1][1]
  end

  def next_emea_dri
    @future_schedule[1][2]
  end

  def next_amer_dri
    @future_schedule[1][3]
  end

  def current_dris
    current_dris = {}

    @name_list.each do |person|
      name = person['name']
      handle = person['handle']
      if apac_dri == name
        current_dris[:apac] = { name: name, handle: handle }
      elsif emea_dri == name
        current_dris[:emea] = { name: name, handle: handle }
      elsif amer_dri == name
        current_dris[:amer] = { name: name, handle: handle }
      end
    end

    current_dris
  end

  def next_dris
    next_dris = {}

    @name_list.each do |person|
      name = person['name']
      handle = person['handle']
      if next_apac_dri == name
        next_dris[:apac] = { name: name, handle: handle }
      elsif next_emea_dri == name
        next_dris[:emea] = { name: name, handle: handle }
      elsif next_amer_dri == name
        next_dris[:amer] = { name: name, handle: handle }
      end
    end

    next_dris
  end

  def assignee_ids
    assignee_ids = []

    current_dris.each_value do |value|
      user = @client.get_user_id_by_handle(value[:handle])
      assignee_ids << user.first.fetch("id")
    end

    assignee_ids
  end

  def timezone_dris_table
    labels = ['Week of', 'APAC', 'EMEA', 'AMER']
    data = [
      [
        start_date,
        "#{current_dris[:apac][:name]} (@#{current_dris[:apac][:handle]})",
        "#{current_dris[:emea][:name]} (@#{current_dris[:emea][:handle]})",
        "#{current_dris[:amer][:name]} (@#{current_dris[:amer][:handle]})"
      ],
      [
        next_start_date,
        "#{next_dris[:apac][:name]} (@#{next_dris[:apac][:handle]})",
        "#{next_dris[:emea][:name]} (@#{next_dris[:emea][:handle]})",
        "#{next_dris[:amer][:name]} (@#{next_dris[:amer][:handle]})"
      ]
    ]

    "#### DRI Schedule\n#{ MarkdownTables.make_table(labels, data, is_rows: true) }"
  end

  def open_handover_report_issue
    @client.get_latest_triage_issue
  end

  def all_notes_from_handover_report
    @client.get_notes_from_issue(open_handover_report_issue.iid)
  end

  def carryovers
    issues = []
    carryovers = []
    format = "https://gitlab.com/gitlab-org/gitlab/-/issues/"

    all_notes_from_handover_report.each do |note|
      if note.body.include?("|:-|:-|:-|:-|:-|")
        links = URI.extract(note.body, %w[https]).to_a
        links.each do |link|
          if link.include?(format) && !link.include?("#stack-trace") && !issues.include?(link)
            issues << link
          end
        end
      end
    end

    issues.each do |issue|
      iid = issue.scan(/\d/).join('')
      if @client.issue_opened?(iid)
        carryovers << "* #{issue}+"
      end
    end

    carryovers
  end

  def new_triage_report(template, assignees)
    title = "Pipeline Triage Report from #{start_date} to #{end_date}"
    @client.open_triage_issue(title, template, assignees)
  end

  def close_past_triage_report
    @client.close_past_triage_report
  end
end
